using Microsoft.VisualStudio.TestTools.UnitTesting;
using ABT_Test___Mars_Rover;

namespace MarsRoverUnitTests
{
    /// <summary>
    /// This test class is designed to test the CommandCentre Class
    /// </summary>
    [TestClass]
    public class CommandCentreTests
    {
        /// <summary>
        /// This method will test the behaviour of the turn rover command.
        /// It will ensure it will move to the expected orientation when turning left. 
        /// </summary>
        [TestMethod]
        public void doesRoverTurnLeftCorrectly()
        {
            MarsRover marsRover = new MarsRover();
            CommandCentre commandCentre = new CommandCentre(marsRover);

            commandCentre.turnRover("Left");
            Assert.AreEqual("East", marsRover.getOrientation());
        }

        /// <summary>
        /// This method will test the behaviour of the turn rover command.
        /// It will ensure it will move to the expected orientation when turning right. 
        /// </summary>
        [TestMethod]
        public void doesRoverTurnRightCorrectly()
        {
            MarsRover marsRover = new MarsRover();
            CommandCentre commandCentre = new CommandCentre(marsRover);

            commandCentre.turnRover("Right");
            Assert.AreEqual("West", marsRover.getOrientation());
        }

        /// <summary>
        /// This method will test the behaviour of the turn rover command.
        /// It will ensure the rover will return to its starting orientation when turning left 4 times.
        /// </summary>
        [TestMethod]
        public void canRoverTurnLeftInACircle()
        {
            MarsRover marsRover = new MarsRover();
            CommandCentre commandCentre = new CommandCentre(marsRover);

            commandCentre.turnRover("Left");
            commandCentre.turnRover("Left");
            commandCentre.turnRover("Left");
            commandCentre.turnRover("Left");
            Assert.AreEqual("South", marsRover.getOrientation());
        }

        /// <summary>
        /// This method will test the behaviour of the turn rover command.
        /// It will ensure the rover will return to its starting orientation when turning right 4 times.
        /// </summary>
        [TestMethod]
        public void canRoverTurnRightInACircle()
        {
            MarsRover marsRover = new MarsRover();
            CommandCentre commandCentre = new CommandCentre(marsRover);

            commandCentre.turnRover("Right"); 
            commandCentre.turnRover("Right");
            commandCentre.turnRover("Right");
            commandCentre.turnRover("Right");

            Assert.AreEqual("South", marsRover.getOrientation());
        }

        /// <summary>
        /// This method will test the isSquareValid method. 
        /// This will ensure that it returns true when a valid square number is entered
        /// </summary>
        [TestMethod]
        public void canRoverMoveToValidSquare()
        {
            MarsRover marsRover = new MarsRover();
            CommandCentre commandCentre = new CommandCentre(marsRover);
            Assert.IsTrue(commandCentre.isNewSquareValid(20));
        }

        /// <summary>
        /// This method will test the isSquareValid method. 
        /// This will ensure that it returns false when an invalid square number is entered
        /// </summary>
        [TestMethod]
        public void willRoverStopWhenMovingToInvalidSquare()
        {
            MarsRover marsRover = new MarsRover();
            CommandCentre commandCentre = new CommandCentre(marsRover);
            Assert.IsFalse(commandCentre.isNewSquareValid(-1));
        }

        /// <summary>
        /// This method will test the calculateNewSquare method.
        /// It will ensure that the method can caluclate the new square the rover should move to correctly 
        /// </summary>
        [TestMethod]
        public void isNewSquareCalculatedCorrectly()
        {
            int expectedValue = 101;
            MarsRover marsRover = new MarsRover();
            CommandCentre commandCentre = new CommandCentre(marsRover);
            int testPosition = commandCentre.calculateNewSquare(1);

            Assert.AreEqual(expectedValue, testPosition);
        }
    }
}
