﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ABT_Test___Mars_Rover
{
    /// <summary>
    /// This class is designed to handle the commands issued to the MarsRover.
    /// The commands will be movement and turning left or right.
    /// The command centre will also be able to return the current position of the MarsRover
    /// </summary>
    public class CommandCentre
    {
        //Flag to monitor when the rover moves out of bounds
        private Boolean roverOutOfBounds = false;

        //Integers representing the limits the rover can travel within 
        private int upperLimit = 0;
        private int lowerLimit = 10000;
        private int leftLimit = 1;
        private int rightLimit = 100;

        //Storage for the commands issued to the rover
        private List<String> storedCommands;

        //Defined limit for the maximum amount of commands that can be issued. 
        private int maxNumberOfCommands = 5;

        //Represents the rover being controlled by the command centre
        private MarsRover marsRover;
        
        /// <summary>
        /// Constructor for the Command Centre.
        /// </summary>
        /// <param name="marsRover"> Represents the rover being controlled by the command centre</param>
        public CommandCentre(MarsRover marsRover)
        {
            this.marsRover = marsRover;
            storedCommands = new List<string>();
        }

        //Returns a string representing the rovers current position and orientation
        public String getRoversPosition()
        {
            return "The rover is currently at position " + marsRover.getCurrentSquare() + " " + marsRover.getOrientation(); 
        }

        /// <summary>
        /// This method will store up to five commands into the system.
        /// </summary>
        public void storeCommands(String commands)
        {
            storedCommands.Clear();
            String[] commandsToStore = commands.Split(",");

            if(commandsToStore.Length > maxNumberOfCommands)
            {
                Console.WriteLine("Warning: Maximum Amount of Commands Reached. Executing first five commands");
            }

            for(int i = 0; i < commandsToStore.Length; i++)
            {
                storedCommands.Add(commandsToStore[i].Trim());
                Console.WriteLine("Command " + (i + 1) + ": " + storedCommands[i]);

                if(i == maxNumberOfCommands - 1)
                {
                    i = commandsToStore.Length;
                }
            }
        }
    
        /// <summary>
        /// This method will process each command one at a time.
        /// It will check for which command to be executed and alert the user if the command isn't recognised.
        /// </summary>
        public void processCommands()
        {
            roverOutOfBounds = false;

            for (int i = 0; i < storedCommands.Count; i++)
            {
                if(roverOutOfBounds)
                {
                    Console.WriteLine("Remaining Commands Skipped Due to Rover moving out of bounds");
                    return;
                }
                
                if(storedCommands[i].ToLower().Contains("left") || storedCommands[i].ToLower().Contains("right"))
                {
                    turnRover(storedCommands[i].ToLower());
                }

                else if(storedCommands[i].ToLower().Contains("m"))
                {
                    moveRover(storedCommands[i].ToLower());
                }

                else
                {
                    Console.WriteLine("Command (" + storedCommands[i] + ") Not Recognised. Skipping to next command");
                }
            }

            Console.WriteLine("End of command sequence");
        }

        /// <summary>
        /// This method is designed to handle when the rover is instructed to move.
        /// The method will call a method to calculate where the rover should move to.
        /// </summary>
        public void moveRover(String command)
        {
            string[] commandSplit = command.Split("m");
            string distanceToMove = commandSplit[0]; 

            if(int.TryParse(distanceToMove, out int sucesss))
            {
                int newPosition = calculateNewSquare(int.Parse(distanceToMove));

                if(isNewSquareValid(newPosition))
                {
                    marsRover.setCurrentSquare(newPosition);
                    leftLimit = setLeftBounds(int.Parse(distanceToMove));
                    rightLimit = setRightBounds(int.Parse(distanceToMove));
                }

                else
                {
                    Console.WriteLine("This movment puts the rover out of bounds (command (" + command +  ") not executed)");
                    roverOutOfBounds = true;
                }
            }

            else
            {
                Console.WriteLine("Distanced not reconised please type an integer (command skipped)");
            }

        }

        /// <summary>
        /// If the mover moves up or down, this method will change the furthest left square that the rover can go.
        /// </summary>
        /// <returns>An integer representing the furthest left the rover can move</returns>
        public int setLeftBounds(int distanceToMove)
        {
            int currentLeftBounds = leftLimit;

            if(marsRover.getOrientation().Equals("North"))
            {
                currentLeftBounds = currentLeftBounds - (100 * distanceToMove);
            }

            else if(marsRover.getOrientation().Equals("South"))
            {
                currentLeftBounds = currentLeftBounds + (100 * distanceToMove);
            }

            return currentLeftBounds;
        }

        /// <summary>
        /// If the mover moves up or down, this method will change the furthest right square that the rover can go.
        /// </summary>
        /// <returns>An integer representing the furthest right the rover can move</returns>
        public int setRightBounds(int distanceToMove)
        {
            int currentRightBounds = rightLimit;

            if (marsRover.getOrientation().Equals("North"))
            {
                currentRightBounds = currentRightBounds - (100 * distanceToMove);
            }

            else if (marsRover.getOrientation().Equals("South"))
            {
                currentRightBounds = currentRightBounds + (100 * distanceToMove);
            }

            return currentRightBounds;
        }

        /// <summary>
        /// This method will calculate the square the rover should move to. 
        /// </summary>
        public int calculateNewSquare(int distanceToMove)
        {
            int newPosition = marsRover.getCurrentSquare();
            
            if(marsRover.getOrientation().Contains("North"))
            {
                newPosition = newPosition - (100 * distanceToMove);
            }

            else if (marsRover.getOrientation().Contains("East"))
            {
                newPosition = newPosition + (1 * distanceToMove);
            }

            else if (marsRover.getOrientation().Contains("South"))
            {
                newPosition = newPosition + (100 * distanceToMove);
            }

            else if (marsRover.getOrientation().Contains("West"))
            {
                newPosition = newPosition - (1 * distanceToMove);
            }

            return newPosition; 
        }

        /// <summary>
        /// This method will check if the square that the rover should move to is within the area of the 100x100 grid
        /// </summary>
        /// <param name="newPosition">a value representing the square the rover should travel to</param>
        /// <returns>true if the square can be travelled to</returns>
        /// <returns>false if the square can't be travelled to</returns>
        public Boolean isNewSquareValid(int newPosition)
        {
            if (marsRover.getOrientation().Equals("North") || marsRover.getOrientation().Equals("South"))
            {
                if(newPosition < upperLimit || newPosition > lowerLimit)
                {
                    return false; 
                }

                else
                {
                    return true;
                }
            }

            else if (marsRover.getOrientation().Equals("East") || marsRover.getOrientation().Equals("West"))
            {
                if (newPosition < leftLimit || newPosition > rightLimit)
                {
                    return false;
                }

                else
                {
                    return true; 
                }
            }

            return false; 
        }

        /// <summary>
        /// This method will change the orientation of the rover based on whether it turns left or right
        /// </summary>
        /// <param name="turningDirection"> represents the direction the rover will turn</param>
        public void turnRover(String turningDirection)
        {
            String currentOrientation = marsRover.getOrientation();

            if(turningDirection.ToLower().Contains("left"))
            {
                switch(currentOrientation)
                {
                    case "North":
                        marsRover.setOrientation("West");
                        break;

                    case "East":
                        marsRover.setOrientation("North");
                        break;

                    case "South":
                        marsRover.setOrientation("East");
                        break;

                    case "West":
                        marsRover.setOrientation("South");
                        break; 
                }
            }

            else if (turningDirection.ToLower().Contains("right"))
            {
                switch (currentOrientation)
                {
                    case "North":
                        marsRover.setOrientation("East");
                        break;

                    case "East":
                        marsRover.setOrientation("South");
                        break;

                    case "South":
                        marsRover.setOrientation("West");
                        break;

                    case "West":
                        marsRover.setOrientation("North");
                        break;
                }
            }
        }
        
    }
}
