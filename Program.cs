﻿using System;

namespace ABT_Test___Mars_Rover
{
    /// <summary>
    /// This is the main class for running the application
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Boolean exitProgram = false;
            MarsRover marsRover = new MarsRover();
            CommandCentre commandCentre = new CommandCentre(marsRover);
           
            Console.WriteLine("Welcome to the Mars Rover Control Program!\n");

            while(exitProgram == false)
            {
                Console.WriteLine("Type help for information on how to operate the rover.");
                Console.WriteLine("Type exit to close the program.\n");
                Console.WriteLine(commandCentre.getRoversPosition());
                Console.WriteLine("Enter Commands to execute (Separate Multiple commands using ','): ");
                
                String commands = Console.ReadLine();
                
                switch(commands.ToLower())
                {
                    case "help":
                        Console.WriteLine("You can enter up to five commands. To delcare multiple commands," +
                            " separate the commands using ','\n");
                        Console.WriteLine("To move the rover enter a distance followed by m e.g. '50m'" +
                            " it will move in a direction based on the rover's orientation\n");
                        Console.WriteLine("To turn the rover, type 'Left' or 'Right'\n");
                        break;

                    case "exit":
                        Console.WriteLine("Thank you for using this software! Goodbye!");
                        exitProgram = true;
                        break;

                    default:
                        commandCentre.storeCommands(commands);
                        commandCentre.processCommands();
                        break;
                }
                
            }
        }
    }
}
