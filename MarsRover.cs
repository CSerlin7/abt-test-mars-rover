﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ABT_Test___Mars_Rover
{
    /// <summary>
    /// This class represents a Mars Rover object that will be controlled by the command centre.
    /// The current square is the rovers position on the 100 x 100 grid
    /// The orientation represents where the rover is facing (i.e. North, East, South, West)
    /// </summary>
    public class MarsRover
    {
        // Values representing the Rover's current location and the direction the rover is facing.
        private int currentSquare;
        private string orientation; 
    
        // Constructor, this sets the starting location and orientation (Square 1 and South).  
        public MarsRover()
        {
            currentSquare = 1;
            orientation = "South";
        }

        // Getters and Setters for the currentSquare
        public void setCurrentSquare(int newSquare)
        {
            currentSquare = newSquare;
        }

        public int getCurrentSquare()
        {
            return currentSquare;
        }

        // Getters and Setters for the orientation
        public void setOrientation(String newOrientation)
        {
            orientation = newOrientation;
        }

        public String getOrientation()
        {
            return orientation;
        }

    }
}
